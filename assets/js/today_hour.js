function validarNumero(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla === 8)
        return true;
    if (tecla === 48)
        return true;
    if (tecla === 49)
        return true;
    if (tecla === 50)
        return true;
    if (tecla === 51)
        return true;
    if (tecla === 52)
        return true;
    if (tecla === 53)
        return true;
    if (tecla === 54)
        return true;
    if (tecla === 55)
        return true;
    if (tecla === 56)
        return true;
    if (tecla === 57)
        return true;
    patron = /1/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

$(".solo_numeros").keypress(function (e) {
    return validarNumero(e);
});

function startTime() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    var s = today.getSeconds();

    // Añadiendo cero si el numero es menor a 10
    h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);

    //Observar si es PM o AM
    //var day_or_night = (h > 11) ? "PM" : "AM";
    var day_or_night = "";

    //convertir 12 horas al sistema.
    // if (h > 12)
    //     h -= 12;

    //Añadiendo tiempo para mostrarlo y actualizar cada 500 milisegundos
    $('#time').html(h + ":" + m + ":" + s + " " + day_or_night);
    setTimeout(function () {
        startTime();
    }, 500);
}

function checkTime(i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}


function limpiar_() {
    $(".login-content").removeClass('color-persona-success-body color-persona-warning-body color-persona-error-body');
    $("#l-lockscreen").removeClass('color-persona-success-lockscreen color-persona-warning-lockscreen color-persona-error-lockscreen')
    $(".mensaje_usuario").empty();
}

var esconde_alerta;
function esconder() {
    esconde_alerta = setTimeout(function () {
        limpiar_();
        $('.verifica_foto').prop('src', CI.base_url + 'assets/img/profile-pics/imagen-no-disponible.jpg');
    }, 7000);
}

$("#send_dni").on("keyup", function () {
    var dni = $(this).val();
    if (dni.length === 8) {
        $.ajax({
            url: CI.base_url + "visita/registrar",
            type: 'POST',
            data: {send_dni: dni},
            dataType: 'json',
            beforeSend: function () {
                clearTimeout(esconde_alerta);
                $("#send_dni").val("");
                $('.verifica_foto').prop('src', CI.base_url + 'assets/img/profile-pics/imagen-no-disponible.jpg');
                limpiar_();
                $(".mensaje_usuario").html('<div class="text-center"><span class="zmdi zmdi-rotate-right zmdi-hc-spin zmdi-hc-3x"></span></div>');
            },
            success: function (response) {
                console.log(response);
				var message = response.message;
				
				if(message.type == "danger"){
					$(".login-content").addClass('color-persona-error-body');
                    $("#l-lockscreen").addClass('color-persona-error-lockscreen');
                    $('.verifica_foto').prop('src', CI.base_url + 'assets/img/profile-pics/imagen-no-disponible.jpg');
                    $(".mensaje_usuario").html(
                            '<div class="col-sm-12">'+
                                '<label for="" class="col-xs-12 text-center f-s-40">'+message.text+'</label>'+
                            '</div>');
                    esconder();
				}else{
					var personal = response.personal;
					
					$(".login-content").addClass('color-persona-'+message.type+'-body');
                    $("#l-lockscreen").addClass('color-persona-'+message.type+'-lockscreen');
                    $('.verifica_foto').prop('src', 'http://iinei.inei.gob.pe/iinei/asistencia/fotos/'+personal.dni+'.jpg');
                    $('.verifica_foto').error(function () {
                        $(this).prop('src', CI.base_url + 'assets/img/profile-pics/imagen-no-disponible.jpg');
                    });
                    $(".mensaje_usuario").html(
                        '<div class="col-sm-12">'+
                            '<label for="" class="col-xs-12 text-center f-s-20">'+message.text+'</label>'+
                        '</div>'+
						'<div class="col-sm-12">'+
                            '<label for="" class="col-xs-12 text-center f-s-30">'+personal.nombres_apellidos+'</label>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<label for="" class="col-xs-12 text-center f-s-25">DNI '+personal.dni+'</label>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">PROYECTO</label>'+
                                '<label for="" class="col-xs-9">'+personal.proyecto_nombre_corto+'</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">SEDE</label>'+
                                '<label for="" class="col-xs-9">'+personal.sede_nombre+'</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">AREA</label>'+
                                '<label for="" class="col-xs-9">'+personal.area_nombre+'</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">CARGO</label>'+
                                '<label for="" class="col-xs-9">'+personal.cargo_nombre+'</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">HORA DE INGRESO</label>'+
                                '<label for="" class="col-xs-9">'+response.hora_ingreso+'</label>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-12">'+
                            '<div class="form-group fg-line">'+
                                '<label class="col-xs-3 show_puntos_" for="">HORA DE SALIDA</label>'+
                                '<label for="" class="col-xs-9">'+(response.hora_salida != null ? response.hora_salida : "")+'</label>'+
                            '</div>'+
                        '</div>'
						);
                    esconder();
				}
				
				
				
                
            }
        });
    }
});
