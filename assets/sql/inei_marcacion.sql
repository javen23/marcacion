/*  tbl_area */
CREATE TABLE tbl_area (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `nombre_corto` varchar(100) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('DIRECCIÓN Y GERENCIA', 'direccion-y-gerencia', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('SEGMENTACIÓN', 'segmentacion', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('METODOLOGÍA Y CAPACITACIÓN', 'metodologia-y-segmentacion', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('OPERACIÓN DE CAMPO', 'operacion-de-campo', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('UNIDAD DE DISTRIBUCIÓN RECEPCIÓN Y ARCHIVO', 'unidad-de-distribucion-recepcion-y-archivo', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('IMPRESIÓN, MODULADO Y EMBALAJE', 'impresion-modulado-y-embalaje', 1);
*/
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('SERVICIO 1', 'servicio-1', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('SERVICIO 2', 'servicio-2', 1);
INSERT INTO tbl_area (nombre, nombre_corto, estado) VALUES('EVALUACION DE DOCENTES', 'evaluacion-docentes', 1);

/*  tbl_cargo */
CREATE TABLE `tbl_cargo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tbl_area_id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, '(ESPECIALISTA DE CONTROL DE CALIDAD)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, '(ANALISTA CONTROL DE CALIDAD/IMPRENTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ANALISTA DE CONTROL DE CALIDAD', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ASISTENTE ADMINISTRATIVO IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ASISTENTE DE COORDINADOR DE CONTROL DE CALIDAD / IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'COORDINADOR DE OPERACIÓN DE CAMPO LIMA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ESPECIALISTA DE CONTROL DE CALIDAD', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'JEFE DE EQUIPO', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'JEFE DE EQUIPO DE CONTROL DE CALIDAD / IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'SUPERVISOR DE ALMACEN', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'SUPERVISOR DE LINEA/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'SUPERVISOR DE UDRA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'TECNICO DE INVENTARIO', 1);

INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, '(ANALISTA CONTROL DE CALIDAD/IMPRENTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'ANALISTA DE CONTROL DE CALIDAD', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'ASISTENTE ADMINISTRATIVO_IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'COORDINADOR DE OPERACIÓN DE CAMPO LIMA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'JEFE DE EQUIPO DE CONTROL DE CALIDAD / IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'SUPERVISOR DE ALMACEN', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'SUPERVISOR DE LINEA/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'TECNICO DE INVENTARIO', 1);

INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, '(ESPECIALISTA DE CONTROL DE CALIDAD)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, '(ANALISTA CONTROL DE CALIDAD/IMPRENTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'ANALISTA DE CONTROL DE CALIDAD', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'ASISTENTE ADMINISTRATIVO_IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'COORDINADOR DE OPERACIÓN DE CAMPO LIMA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'JEFE DE EQUIPO', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'JEFE DE EQUIPO DE CONTROL DE CALIDAD / IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'SUPERVISOR DE ALMACEN', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'SUPERVISOR DE LINEA/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'TECNICO DE INVENTARIO', 1);

/*
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ECONOMICO SOCIAL V( COORDINADOR GENERAL DEL PROYECTO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ECONOMICO SOCIAL V (JEFE DE METODOLOGIA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ECONOMICO SOCIAL III (ASESOR METODOLOGICO DEL PROYECTO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ADMINISTRATIVO III ( JEFE DE ADMINISTRACION)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ADMINISTRATIVO I (COORDINADOR DE RECURSOS HUMANOS)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'PROFESIONAL ADMINISTRATIVO I (ASISTENTE ADMINISTRATIVO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ASISTENTE ESPECIALISTA ADMINISTRATIVO V (ASISTENTE ADMINISTRATIVO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(1, 'ASISTENTE ESPECIALISTA ADMINISTRATIVO IV (ASISTENTE ADMINISTRATIVO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'PROFESIONAL ECONOMICO SOCIAL II (JEFE DE SEGMENTACION)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'TECNICO ECONOMICO SOCIAL III ( SEGMENTISTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'PROFESIONAL ECONOMICO SOCIAL II (RESPONSABLE DE PROGRAMACION TERRITORIAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'TECNICO INFORMATICO VII (ANALISTA DE PROGRAMACION DE RUTAS)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'TECNICO ECONOMICO SOCIAL IV (PROGRAMADOR DE RUTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(2, 'TECNICO ECONOMICO SOCIAL III (OPERADOR)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'PROFESIONAL ECONOMICO SOCIAL I (METODOLOGO C)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL V (ASISTENTE DE CAPACITACION E INFORMES)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'TECNICO INFORMATICO  VI (ASISTENTE DE CAPACITACION E INFORMES)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL IV (ASISTENTE DE CAPACITACION E INFORMES)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'PROFESIONAL INFORMATICO III (INFORMATICO 1)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'TECNICO INFORMATICO V (ASISTENTE INFORMATICO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'PROFESIONAL ECONOMICO SOCIAL IV (JEFE DE CAMPO NACIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'PROFESIONAL ECONOMICO SOCIAL III (ESPECIALISTA DE OPERACIÓN CAMPO)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'PROFESIONAL ECONOMICO SOCIAL I (COORDINADOR DE MONITOREO NACIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL V (ESPECIALISTA DE MONITOREO - LIMA-NACIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL IV (MONITOR NACIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'PROFESIONAL ECONOMICO SOCIAL I (COORDINADOR REGIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'PROFESIONAL ECONOMICO SOCIAL II (COORDINADOR DE SUPERVISORES NACIONALES)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL V (SUPERVISOR NACIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'ASISTENTE ESPECIALISTA ECONOMICO SOCIAL III (SUPERVISOR DISTRITAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'TECNICO ADMINISTRATIVO IV (ASISTENTE ADMINISTRATIVO REGIONAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(4, 'TECNICO ADMINISTRATIVO III ( ASISTENTE ADMINISTRATIVO DISTRITAL)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(5, 'TECNICO ECONOMICO SOCIAL IV ( TECNICO DE ARCHIVO A)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(5, 'TECNICO ECONOMICO SOCIAL III (TECNICO DE ARCHIVO B)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(5, 'TECNICO ECONOMICO SOCIAL II (TECNICO DE ARCHIVO C)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(5, 'JEFE DE UDRA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'PROFESIONAL INFORMÁTICO VII (JEFE DE CONTROL DE CALIDAD IMPRENTA)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(3, 'PROFESIONAL INFORMÁTICO II (INFORMÁTICO 1)', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'SUPERVISOR DE LINEA IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'OPERADOR DE IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'ANALISTA DE CONTROL DE CALIDAD/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'SUPERVISOR DE LÍNEA/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'JEFE DE EQUIPO - CONTROL DE CALIDAD/IMPRENTA', 1);
INSERT INTO `tbl_cargo` (`tbl_area_id`, `nombre`, `estado`) VALUES(6, 'JEFE CONTROL DE CALIDAD/IMPRENTA', 1);
*/
/*  tbl_perfil */
CREATE TABLE `tbl_perfil` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('SUPER ADMIN', 1);
INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('ADMINISTRADOR', 1);
INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('DIGITADOR', 1);
INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('CONSULTA', 1);
INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('APP', 1);
INSERT INTO `tbl_perfil` (`nombre`, `estado`) VALUES('SEDE', 1);

/*  tbl_proyecto */
CREATE TABLE `tbl_proyecto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text,
  `nombre_corto` varchar(45) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_proyecto` (`nombre`, `nombre_corto`, `estado`) VALUES('EVALUACIÓN CENSAL DE ESTUDIANTES 2016', 'ece-2016', 1);
INSERT INTO `tbl_proyecto` (`nombre`, `nombre_corto`, `estado`) VALUES('EVALUACIÓN PARA LOS CONCURSOS PÚBLICOS DE INGRESO A LA CARRERA PÚBLICA MAGISTERIAL Y DE CONTRATACIÓN DOCENTE EN INSTITUCIONES EDUCATIVAS PÚBLICAS DE EDUCACIÓN BÁSICA - 2016', 'ednom-2016', 1);
INSERT INTO `tbl_proyecto` (`nombre`, `nombre_corto`, `estado`) VALUES('CAS', 'cas', 1);

/*  tbl_sede */
CREATE TABLE `tbl_sede` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(80) DEFAULT NULL,
  `nombre_corto` varchar(80) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_sede` (`nombre`, `nombre_corto`, `estado`) VALUES('CERVANTES', 'cervantes', 1);
INSERT INTO `tbl_sede` (`nombre`, `nombre_corto`, `estado`) VALUES('GARZON', 'garzon', 1);
INSERT INTO `tbl_sede` (`nombre`, `nombre_corto`, `estado`) VALUES('MARQUEZ', 'marquez', 1);
INSERT INTO `tbl_sede` (`nombre`, `nombre_corto`, `estado`) VALUES('SALESIANO', 'salesiano', 1);
INSERT INTO `tbl_sede` (`nombre`, `nombre_corto`, `estado`) VALUES('IMPRENTA', 'imprenta', 1);

/*  tbl_usuario_sede */
CREATE TABLE `tbl_usuario_sede` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tbl_perfil_id` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `clave` varchar(40) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_usuario_sede` (`tbl_perfil_id`, `nombre`, `clave`) VALUES(6, 'sede_cervantes', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `tbl_usuario_sede` (`tbl_perfil_id`, `nombre`, `clave`) VALUES(6, 'sede_garzon', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `tbl_usuario_sede` (`tbl_perfil_id`, `nombre`, `clave`) VALUES(6, 'sede_marquez', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `tbl_usuario_sede` (`tbl_perfil_id`, `nombre`, `clave`) VALUES(6, 'sede_salesiano', '7c4a8d09ca3762af61e59520943dc26494f8941b');
INSERT INTO `tbl_usuario_sede` (`tbl_perfil_id`, `nombre`, `clave`) VALUES(6, 'sede_imprenta', '7c4a8d09ca3762af61e59520943dc26494f8941b');

/*  usuario_cargo */
CREATE TABLE `usuario_cargo` (
  `usuario_id` int(10) DEFAULT NULL,
  `cargo_id` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `usuario_cargo` (`usuario_id`, `cargo_id`) VALUES(2, 39);
INSERT INTO `usuario_cargo` (`usuario_id`, `cargo_id`) VALUES(2, 40);
INSERT INTO `usuario_cargo` (`usuario_id`, `cargo_id`) VALUES(2, 41);
INSERT INTO `usuario_cargo` (`usuario_id`, `cargo_id`) VALUES(2, 42);
INSERT INTO `usuario_cargo` (`usuario_id`, `cargo_id`) VALUES(2, 43);

/*  tbl_usuario */
CREATE TABLE `tbl_usuario` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tbl_persona_id` int(10) UNSIGNED DEFAULT NULL,
  `tbl_perfil_id` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL,
  `clave` varchar(40) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `estado_app` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tbl_usuario` (`tbl_perfil_id`, `nombre`, `clave`, `estado`) VALUES(1, 'probando', '41e644663b5db8a90ad3619e3235278b29f7c94a', 1);  /* Inei.123 */
INSERT INTO `tbl_usuario` (`tbl_perfil_id`, `nombre`, `clave`, `estado`) VALUES(4, 'consulta', '7c4a8d09ca3762af61e59520943dc26494f8941b', 1); /* 123456 */

/*  tbl_persona */
CREATE TABLE `tbl_persona` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tbl_proyecto_id` int(11) NOT NULL,
  `tbl_cargo_id` int(10) UNSIGNED NOT NULL,
  `tbl_sede_id` int(10) UNSIGNED NOT NULL,
  `nombres_apellidos` varchar(200) DEFAULT NULL,
  `nombres_cadena` varchar(200) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `email` varchar(80) DEFAULT NULL,
  `telefono_inei` varchar(10) DEFAULT NULL,
  `telefono_personal` varchar(10) DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `tbl_persona`
  ADD KEY `fk_tbl_persona_tbl_cargo1_idx` (`tbl_cargo_id`),
  ADD KEY `fk_tbl_persona_tbl_sede1_idx` (`tbl_sede_id`),
  ADD KEY `fk_tbl_persona_tbl_proyecto1_idx` (`tbl_proyecto_id`);




/*  tbl_visita */
CREATE TABLE `tbl_visita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dni` varchar(8) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `hora_ingreso` time DEFAULT NULL,
  `hora_salida` time DEFAULT NULL,
  `estado` int(11) DEFAULT '1',
  `creado_por` varchar(20) DEFAULT 'root',
  `fecha_creado` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modificado_por` varchar(20) DEFAULT NULL,
  `fecha_modificado` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

