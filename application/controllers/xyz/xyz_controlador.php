<?php
date_default_timezone_set('America/Lima');

if(!defined('BASEPATH'))exit('No direct script access allowed');

class Xyz_controlador extends CI_Controller{
	public function __construct(){
		parent::__construct();
        //$this->load->model('personal/personal_model', 'modeloPersonal');
		$this->load->model('xyz/xyz_modelo', 'model');
        //$this->load->library('csvimport');
        //$this->load->library('upload');
        //ini_set("memory_limit", -1);
        //ini_set('max_execution_time', -1);
        //$this->nameUsuario = $this->session->userdata('userNameUsuario');
        //$this->if ad_rol       = $this->session->userdata('userIdRol');
		
		//$this->load->model('asistencia/asistencia_model', 'modeloAsistencia');
        //$this->load->model('personal/personal_model', 'modeloPersonal');
        $this->id_rol   = $this->session->userdata('userIdRol');
		
        /*if ($this->id_rol == 6) {
            redirect('visita', "refresh");
        }*/
	}
	
	public function index(){
		
	}
	
	public function personal(){
		add_css(array('sweetalert/sweetalert-override.min',
            'datatables/dataTables.bootstrap',
            'bootstrap-select/bootstrap-select.min'));
        add_js(array('plugins/sweetalert/sweetalert.min',
            'plugins/jquery-ui/jquery-ui.min',
            'plugins/bootstrap-select/bootstrap-select.min',
            'plugins/bootstrap-select/i18n/defaults-es_CL.min',
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'plugins/jqueryvalidation/dist/jquery.validate'));
		
		//hacer una consulta de acuerdo a los cargos que hayan sido asignados al usuario
		$cargos = array();
		$result = $this->model->cargo_por_usuario($this->session->userdata("idUserLogin"));
		for($i = 0; $i < count($result); $i++){
			$cargos[] = $result[$i]["cargo_id"];
		}
		//print_r($result);
		
		
        $datos['personal']    = $this->model->personal_por_cargo($cargos);
        $datos['titulo']            = "Lista de Personal";// se mostrarán de acuerdo al cargo del usuario logeado
        $datos['contenido']         = 'xyz/personal_vista';
        $this->load->view('plantilla', $datos);
	}
	
	public function asistencia(){
		add_css(array('sweetalert/sweetalert-override.min',
			'datatables/dataTables.bootstrap',
			'bootstrap-select/bootstrap-select.min',
			'datetimepicker/build/css/bootstrap-datetimepicker.min'));
		add_js(array('plugins/sweetalert/sweetalert.min',
			'plugins/jquery-ui/jquery-ui.min',
			'plugins/bootstrap-select/bootstrap-select.min',
			'plugins/bootstrap-select/i18n/defaults-es_CL.min',
			'plugins/datatables/jquery.dataTables.min',
			'plugins/datatables/dataTables.bootstrap.min',
			'plugins/moment/min/moment.min',
			'plugins/moment/locale/es',
			
			'plugins/datetimepicker/build/js/bootstrap-datetimepicker.min'));
		$data['listado_sedes']  = $this->model->obtener_sedes();
		$data['listado_areas']  = $this->model->obtener_areas();
		$data['listado_visita'] = $this->model->obtener_listado_visita();
		$data['titulo']         = "Reporte de asistencia";
		$data['contenido']      = "xyz/asistencia_vista";
		$this->load->view('plantilla', $data);
	}
	
	public function asistencia_rango(){
		add_css(array('sweetalert/sweetalert-override.min',
            'datatables/dataTables.bootstrap',
            'bootstrap-select/bootstrap-select.min',
            'datetimepicker/build/css/bootstrap-datetimepicker.min'));
        add_js(array('plugins/sweetalert/sweetalert.min',
            'plugins/bootstrap-select/bootstrap-select.min',
            'plugins/bootstrap-select/i18n/defaults-es_CL.min',
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'plugins/moment/min/moment.min',
            'plugins/moment/locale/es',
            'plugins/datetimepicker/build/js/bootstrap-datetimepicker.min'));

            $data['listado_proyecto']  = $this->model->proyectos();
			//$data['listado_sedes']  = $this->model->sedes();
			//$data['listado_areas']  = $this->model->areas();
			$data['titulo']         = "Asistencia por rango";
			$data['contenido']      = "xyz/asistencia_rango_vista";
			$data['listado_visita'] = $this->model->asistencia_rango($_REQUEST);
			
			
			$time = time();
			$data["desde"] = date("d-m-Y", strtotime("-7 day", $time));
			$data["hasta"] = date("d-m-Y", $time);
			if(isset($_REQUEST["fecha_desde"])){
				$data["desde"] = date("d-m-Y", strtotime($_REQUEST["fecha_desde"]));
			}
			if(isset($_REQUEST["fecha_hasta"])){
				$data["hasta"] = date("d-m-Y", strtotime($_REQUEST["fecha_hasta"]));
			}
			
			if($this->input->is_ajax_request()){
				$this->load->view('xyz/asistencia_rango_vista_ajax', $data);
			}else{
				//$data['listado_visita'] = $this->model->asistencia_rango();
				$this->load->view('plantilla', $data);
			}
			
	}
	
	public function asistencia_dia(){
		add_css(array('sweetalert/sweetalert-override.min',
            'datatables/dataTables.bootstrap',
            'bootstrap-select/bootstrap-select.min',
            'datetimepicker/build/css/bootstrap-datetimepicker.min'));
        add_js(array('plugins/sweetalert/sweetalert.min',
            'plugins/bootstrap-select/bootstrap-select.min',
            'plugins/bootstrap-select/i18n/defaults-es_CL.min',
            'plugins/datatables/jquery.dataTables.min',
            'plugins/datatables/dataTables.bootstrap.min',
            'plugins/moment/min/moment.min',
            'plugins/moment/locale/es',
            'plugins/datetimepicker/build/js/bootstrap-datetimepicker.min'));

            $data['listado_proyecto']  = $this->model->proyectos();
			//$data['listado_sedes']  = $this->model->sedes();
			$data['titulo']         = "Asistencia por día";
			$data['contenido']      = "xyz/asistencia_dia_vista";
			$data['listado_visita'] = $this->model->asistencia_dia();
			$time = time();
			$data["fecha"] = date("d-m-Y", $time);
            $this->load->view('plantilla', $data);
            /*
			if(isset($_REQUEST["fecha"])){
				$data["fecha"] = date("d-m-Y", strtotime($_REQUEST["fecha"]));
			}
			
			if($this->input->is_ajax_request()){
				$this->load->view('xyz/asistencia_dia_vista_ajax', $data);
			}else{
				$this->load->view('plantilla', $data);
			}
			*/
	}

    public function asistencia_dia_filtro(){
        if ($this->input->is_ajax_request()) {
            $time = time();
            $data["fecha"] = date("d-m-Y", $time);
            $data['listado_visita'] = $this->model->obtener_listado_asistencia_dia($_REQUEST);
            $this->load->view('xyz/asistencia_dia_vista_ajax', $data);
        }
    }
	
}


?>