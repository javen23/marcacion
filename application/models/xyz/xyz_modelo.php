<?php
date_default_timezone_set('America/Lima');

if (!defined('BASEPATH')) exit('No direct script access allowed');

class xyz_modelo extends CI_Model{
	public function __construct(){
		parent::__construct();
	}
	
	
	
	// --- generales
    public function proyectos(){
        $sql = "select * from tbl_proyecto";
        $query = $this->db->query($sql);
        return $query->result_array();
    }


	public function sedes(){
		$sql = "select * from tbl_sede";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function areas(){
		$sql = "select * from tbl_area";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function asistencia_dia(){
		$time = time();
		$fecha = date("Y-m-d", $time);
		
		//if(isset($params["fecha"])){
		//	$fecha = date("Y-m-d", strtotime($params["fecha"]));
		//}

		// -- consultar usuario_cargo
		$usuario_id = $this->session->userdata('idUserLogin');
		$query = $this->db->query("select * from usuario_cargo where usuario_id = '".addslashes($usuario_id)."'");
		if($query->num_rows() > 0){
			$where_cargo = "";
			$result = $query->result_array();
			foreach($result as $row){
				$where_cargo .= (strlen($where_cargo) > 0 ? "," : "").$row["cargo_id"];
			}
			
			$sql = "select 
                t3.nombre as nombre_proyecto,
                upper(t3.nombre_corto) as nombre_corto_proyecto,
				t5.nombre as nombre_cargo,
				t6.nombre as nombre_area,                
				t1.nombres_apellidos,
				t1.dni,
				t2.fecha,
				t2.hora_ingreso,
				t2.hora_salida
			from
				(select tbl_proyecto_id,tbl_cargo_id,nombres_apellidos, dni from tbl_persona where tbl_cargo_id in(".$where_cargo.") and estado = 1) t1
				left join (select dni, fecha, hora_ingreso, hora_salida from tbl_visita where fecha = '".addslashes($fecha)."') t2 on t1.dni = t2.dni
				left join tbl_proyecto t3 on t1.tbl_proyecto_id = t3.id
				left join tbl_cargo t5 on t1.tbl_cargo_id = t5.id
				left join tbl_area t6 on t5.tbl_area_id = t6.id							
			order by
				t1.nombres_apellidos";
			
			//echo $sql;
			
			/*$____sql = "select
					t1.dni,
					t1.fecha,
					t2.nombres_apellidos,
					t3.nombre as nombre_proyecto,
					upper(t3.nombre_corto) as nombre_corto_proyecto,
					t4.nombre as nombre_sede,
					t5.nombre as nombre_cargo,
					t6.nombre as nombre_area,
					'0' as dias_visitas,
					'0' as cantidad_visitas,
					t1.fecha as fecha,
					t1.hora_ingreso as hora_ingreso,
					t1.hora_salida as hora_salida
				from
					tbl_visita t1
					left join tbl_persona t2 on t1.dni = t2.dni
					left join tbl_proyecto t3 on t2.tbl_proyecto_id = t3.id
					left join tbl_sede t4 on t2.tbl_sede_id = t4.id
					left join tbl_cargo t5 on t2.tbl_cargo_id = t5.id
					left join tbl_area t6 on t5.tbl_area_id = t6.id
				where
					t2.estado = 1 and t1.fecha between '".addslashes($desde)."' and '".addslashes($hasta)."'".$where_sedes.$where_areas." and t5.id in(".$where_cargo.")
			";*/
			//echo $sql;
			
			$query = $this->db->query($sql);
            if($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return array();
            }

		}else{
			return array();
		}
		
		
	}


    public function obtener_listado_asistencia_dia($params = NULL){
        $time = time();
        $fecha = date("Y-m-d", $time);

        if(isset($params["fecha"])){
            $fecha = date("Y-m-d", strtotime($params["fecha"]));
        }

        // -- consultar usuario_cargo
        $usuario_id = $this->session->userdata('idUserLogin');
        $query = $this->db->query("select * from usuario_cargo where usuario_id = '".addslashes($usuario_id)."'");
        if($query->num_rows() > 0){
            $where_cargo = "";
            $result = $query->result_array();
            foreach($result as $row){
                $where_cargo .= (strlen($where_cargo) > 0 ? "," : "").$row["cargo_id"];
            }

            $sql = "select 
                t3.nombre as nombre_proyecto,
                upper(t3.nombre_corto) as nombre_corto_proyecto,
				t5.nombre as nombre_cargo,
				t6.nombre as nombre_area,                 
				t1.nombres_apellidos,
				t1.dni,
				t2.fecha,
				t2.hora_ingreso,
				t2.hora_salida
			from
				(select tbl_proyecto_id,tbl_cargo_id,nombres_apellidos, dni from tbl_persona where tbl_cargo_id in(".$where_cargo.") and estado = 1) t1
				left join (select dni, fecha, hora_ingreso, hora_salida from tbl_visita where fecha = '".addslashes($fecha)."') t2 on t1.dni = t2.dni
				left join tbl_proyecto t3 on t1.tbl_proyecto_id = t3.id
				left join tbl_cargo t5 on t1.tbl_cargo_id = t5.id
				left join tbl_area t6 on t5.tbl_area_id = t6.id					
			where t3.id in (".$params["proyecto"].")	
			order by
				t1.nombres_apellidos";

            $query = $this->db->query($sql);
            return $query->result_array();
        }else{
            return array();
        }


    }

	public function asistencia_rango($params = NULL){
		/*$params = array(
			"sedes" => array(),
			"areas" => array(),
			"desde" => "2016-03-01",
			"hasta" => "2016-03-23"
		);*/
		$time = time();
		$desde = date("Y-m-d", strtotime("-7 day", $time));
		$hasta = date('Y-m-d', $time);
		$where_sedes = "";
		$where_areas = "";
        $where_proyectos = "";

		if(isset($params["fecha_desde"])){
			$desde = date("Y-m-d", strtotime($params["fecha_desde"]));
		}
		if(isset($params["fecha_hasta"])){
			$hasta = date("Y-m-d", strtotime($params["fecha_hasta"]));
		}
        if(isset($params["proyecto"]) && $params["proyecto"] != "null"){
            $where_proyectos = " and t2.tbl_proyecto_id in(".addslashes($params["proyecto"]).")";
        }
		if(isset($params["sedes"]) && $params["sedes"] != "null"){
			$where_sedes = " and t2.tbl_sede_id in(".addslashes($params["sedes"]).")";
		}
		if(isset($params["areas"]) && $params["areas"] != "null"){
			$where_areas = " and t5.tbl_area_id in(".addslashes($params["areas"]).")";
		}
		
		// -- consultar usuario_cargo
		$usuario_id = $this->session->userdata('idUserLogin');
		$query = $this->db->query("select * from usuario_cargo where usuario_id = '".addslashes($usuario_id)."'");
		if($query->num_rows() > 0){
			$where_cargo = "";
			$result = $query->result_array();
			foreach($result as $row){
				$where_cargo .= (strlen($where_cargo) > 0 ? "," : "").$row["cargo_id"];
			}
			
			$sql = "select
					t1.dni,
					t1.fecha,
					t2.nombres_apellidos,
					t3.nombre as nombre_proyecto,
					upper(t3.nombre_corto) as nombre_corto_proyecto,
					t4.nombre as nombre_sede,
					t5.nombre as nombre_cargo,
					t6.nombre as nombre_area,
					'0' as dias_visitas,
					'0' as cantidad_visitas,
					t1.fecha as fecha,
					t1.hora_ingreso as hora_ingreso,
					t1.hora_salida as hora_salida
				from
					tbl_visita t1
					left join tbl_persona t2 on t1.dni = t2.dni
					left join tbl_proyecto t3 on t2.tbl_proyecto_id = t3.id
					left join tbl_sede t4 on t2.tbl_sede_id = t4.id
					left join tbl_cargo t5 on t2.tbl_cargo_id = t5.id
					left join tbl_area t6 on t5.tbl_area_id = t6.id
				where
					t2.estado = 1 and t1.fecha between '".addslashes($desde)."' and '".addslashes($hasta)."'".$where_sedes.$where_areas.$where_proyectos." and t5.id in(".$where_cargo.")
			";
			//echo $sql;
			
			$query = $this->db->query($sql);
			return $query->result_array();
		}else{
			return array();
		}
			
		
					/*
					group_concat(distinct(date_format(t1.fecha, '%d-%m'))) as dias_visitas,
					count(*) as cantidad_visitas
					
					group by
					t1.dni
					*/
				
				
				/*
				-- SELECT DE BETZABE
				
				SELECT
				vis.dni,
				vis.fecha,
				per.nombres_apellidos,
				are.nombre AS nombre_area,
				car.nombre AS nombre_cargo,
				pro.nombre AS nombre_proyecto,
				UPPER(pro.nombre_corto) AS nombre_corto_proyecto,
				sed.nombre AS nombre_sede,
				'0' AS dias_visitas,
				'0' AS cantidad_visitas  FROM
				tbl_visita AS vis
				LEFT JOIN tbl_persona AS per ON vis.dni=per.dni
				LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
				LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
				LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
				LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id WHERE
				vis.fecha BETWEEN '2016-03-22' AND '2016-03-23'
				ORDER BY per.nombres_apellidos, vis.fecha
				*/
					
		///echo $sql;
		
					
		
		/*
			SELECT
				vis.dni,
				vis.fecha,
					per.nombres_apellidos,
				are.nombre AS nombre_area,
				car.nombre AS nombre_cargo,
				pro.nombre AS nombre_proyecto,
			UPPER(pro.nombre_corto) AS nombre_corto_proyecto,
				sed.nombre AS nombre_sede,
			GROUP_CONCAT(DISTINCT(DATE_FORMAT(vis.fecha,'%d-%m'))) AS dias_visitas,
			COUNT(DISTINCT(vis.fecha)) AS cantidad_visitas  FROM
				tbl_visita AS vis
				LEFT JOIN tbl_persona AS per ON vis.dni=per.dni
				LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
				LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
				LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
				LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id WHERE
				1=1
			
			GROUP BY vis.dni
			ORDER BY per.nombres_apellidos
			
			AND CASE WHEN  (vis.hora_salida ='00:00:00') THEN vis.hora_ingreso BETWEEN ('19:00:00') AND ('23:59:59') ELSE vis.hora_salida BETWEEN ('19:00:00') AND ('23:59:59')  END
			AND vis.fecha BETWEEN '2016-03-15' AND '2016-03-22' 
		
		
		
		SELECT
                    vis.dni,
                    vis.fecha,
                    per.nombres_apellidos,
                    are.nombre AS nombre_area,
                    car.nombre AS nombre_cargo,
                    pro.nombre AS nombre_proyecto,
                    UPPER(pro.nombre_corto) AS nombre_corto_proyecto,
                    sed.nombre AS nombre_sede,
                    '0' AS dias_visitas,
                    '0' AS cantidad_visitas  FROM
                    tbl_visita AS vis
                    LEFT JOIN tbl_persona AS per ON vis.dni=per.dni
                    LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
                    LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
                    LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
                    LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id WHERE
                    vis.fecha BETWEEN '2016-03-22' AND '2016-03-23'
                    ORDER BY per.nombres_apellidos, vis.fecha
		*/
	
	}
	
	
	
	// ---
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function cargo_por_usuario($usuario_id){
		$sql = "select * from usuario_cargo where usuario_id = '".addslashes($usuario_id)."'";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
	
	public function personal_por_cargo($cargos = array()){
		if(count($cargos) > 0){
			$sql="SELECT
					per.*,
					car.tbl_area_id,
					are.nombre AS nombre_area,
					car.nombre AS nombre_cargo,
					pro.nombre AS nombre_proyecto,
					pro.nombre_corto AS nombre_corto_proyecto,
					sed.nombre AS nombre_sede,
					sed.nombre_corto AS nombre_corto_sede
				FROM
					tbl_persona AS per
					LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
					LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
					LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
					LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id
				WHERE
					car.id in(".implode(",", $cargos).")
				ORDER BY
					per.nombres_apellidos";
			$query=$this->db->query($sql);
			return $query->result_array();
		}else{
			return array();//SI NO ENCUENTRA DATOS ES PORQUE NO TIENE ASIGNADO CARGOS (usuario_cargo)
		}
	}
}

?>