<?php

if (!defined('BASEPATH'))
exit('No direct script access allowed');

class Visita_model extends CI_Model {

   public function __construct() {
      parent::__construct();
      $this->idLogin        = $this->session->userdata('idUserLogin');
      $this->nameUsuario    = $this->session->userdata('userNameUsuario');
   }
   
   
	public function visita_registrar($dni){
		$response = array(
			"message" => array(
				"type" => "",
				"text" => ""
			),
			"personal" => array(),
			"hora_ingreso" => "",
			"hora_salida" => ""
		);
		
		$q1 = $this->db->query("
			select
				t1.nombres_apellidos, t1.dni, t1.estado, t2.nombre_corto as proyecto_nombre_corto, t3.nombre as sede_nombre, t4.nombre as cargo_nombre, t5.nombre as area_nombre
			from
				tbl_persona t1
				left join tbl_proyecto t2 on t1.tbl_proyecto_id = t2.id
				left join tbl_sede t3 on t1.tbl_sede_id = t3.id
				left join tbl_cargo t4 on t1.tbl_cargo_id = t4.id
				left join tbl_area t5 on t4.tbl_area_id = t5.id
			where
				t1.dni = '".addslashes($dni)."'");
		if($q1->num_rows() == 1){
			$r1 = $q1->row_array();
			$response["personal"] = $r1;
			
			if($r1["estado"] == "1"){
				$q2 = $this->db->query("select *, if(curtime() > date_add(hora_ingreso, interval 5 minute), 1, 0) as ingreso_ok from tbl_visita where dni = '".addslashes($dni)."' and fecha = curdate()");
				$r2 = $q2->row_array();
				
				if($q2->num_rows() == 1){
					$response["hora_ingreso"] = $r2["hora_ingreso"];
					$response["hora_salida"] = $r2["hora_salida"];
					
					if($r2["hora_salida"] == "00:00:00"){
						// verificar que la hora de salida no sea 1 minuto despues
						if($r2["ingreso_ok"] == "1"){
							// registrar salida
							$this->db->query("update tbl_visita set hora_salida = curtime(), estado=2, modificado_por = '".addslashes($this->nameUsuario)."', fecha_modificado = now() where dni = '".addslashes($dni)."' and fecha = curdate()");

                            // Obtenemos la Hora de salida registrado en el Sistema
                            $query1 = $this->db->query("select hora_salida from tbl_visita where fecha = curdate() and dni = '".addslashes($dni)."'");
                            $row1 = $query1->row_array();

                            $response["hora_salida"] = $row1['hora_salida'];
                            //$response["hora_salida"] = date("H:i:s");
							$response["message"]["type"] = "success";
							$response["message"]["text"] = "Hasta pronto";
						}else{
							// antes de un minuto, advertir que no puede registrar la salida sino hasta despues de un minuto
							$response["message"]["type"] = "error";
							$response["message"]["text"] = "Debe esperar al menos 5 minutos para registrar su salida.";
						}
					}else{// ya registró su salida, no hacer nada mas
						$response["message"]["type"] = "error";
						$response["message"]["text"] = "Usted ya registró su salida.";
					}
				}else{
					// CREAR
					$this->db->query("insert into tbl_visita(dni, fecha, hora_ingreso, hora_salida, estado, creado_por, fecha_creado) values('".addslashes($dni)."', curdate(), curtime(),'00:00:00', 1, '".addslashes($this->nameUsuario)."', now())");

                    // Obtenemos la Hora de ingreso registrado en el Sistema
                    $query2 = $this->db->query("select hora_ingreso from tbl_visita where fecha = curdate() and dni = '".addslashes($dni)."'");
                    $row2 = $query2->row_array();

					$response["hora_ingreso"] = $row2["hora_ingreso"];
                    //$response["hora_ingreso"] = date("H:i:s");
					$response["message"]["type"] = "success";
					$response["message"]["text"] = "Bienvenido";
				}
			}else{
				// dni esta deshabilitado
				$response["message"]["type"] = "error";
				$response["message"]["text"] = $r1["nombres_apellidos"]." [".$dni."] no tiene acceso a esta sede.";
			}
		}else{
			// dni no existe
			$response["message"]["type"] = "danger";
			$response["message"]["text"] = "El dni ".$dni." no está registrado.";
		}
		
		return $response;
		
	}
   
   

    public function ___registrar_visita($dni){
        $sql                    = "SELECT dni FROM tbl_persona WHERE dni='{$dni}'";
        $query                  = $this->db->query($sql);
        if($query->num_rows()==1){

            $sql_asistencia     = "SELECT
                                        *
                                    FROM
                                        tbl_visita
                                    WHERE 1=1
                                        AND dni='{$dni}' AND fecha='".date('Y-m-d')."'
                                        AND hora_ingreso !='00:00:00' AND estado=1
                                        AND id=(SELECT max(id) AS max_id FROM tbl_visita WHERE dni='{$dni}') ";
            log_message("INFO","seleccionando para actualizar --- {$sql_asistencia}");
            $query_asistencia   = $this->db->query($sql_asistencia);

            if($query_asistencia->num_rows()>=1){

                $sql_id_max     = "SELECT max(id) AS max_id FROM tbl_visita WHERE dni='".$dni."'";
                log_message("INFO","SELECCIONAR MAX DNI PARA ACTUALIZAR --- {$sql_id_max}");
                $query_id_max   =  $this->db->query($sql_id_max)->row_array();

                $data_hora_fin  = array(
                        'estado'            => 2,
                        'hora_salida'       => date('H:i:s'),
                        'modificado_por'    => $this->nameUsuario,
                        'fecha_modificado'  => date("Y-m-d H:i:s")
                    );
                //$sql_where      = " dni='".$dni."' AND id=".$query_id_max['max_id']." ";
                $this->db->where('dni',$dni);
                $this->db->where('id',$query_id_max['max_id']);
                $this->db->update('tbl_visita',$data_hora_fin);

                $sql_lista      = $this->select_visita();
                $sql_lista     .= " WHERE vis.dni='".$dni."' AND vis.id=".$query_id_max['max_id']." ";
                log_message("INFO","DEVOLVIENDO RESULTADO ACTUALIZADO --- {$sql_lista}");
                $query_persona  = $this->db->query($sql_lista)->row_array();
                $query_persona['recien'] = 2;

            }else{

                $data_hora_inicio = array(
                    'dni'           => $dni,
                    'fecha'         => date('Y-m-d'),
                    'hora_ingreso'  => date('H:i:s'),
                    'hora_salida'   => '00:00:00',
                    'creado_por'    => $this->nameUsuario
                    );

                $this->db->insert('tbl_visita',$data_hora_inicio);
                $ultimo_id      = $this->db->insert_id();

                $sql_lista      = $this->select_visita();
                $sql_lista     .= " WHERE vis.dni={$dni} AND vis.id={$ultimo_id}";
                $query_persona  = $this->db->query($sql_lista)->row_array();
                $query_persona['recien'] = 1;

            }

            return $query_persona;
        }
        return NULL;
    }

    public function select_visita(){
        return "SELECT
                    per.dni,
                    per.nombres_apellidos,
                    are.nombre AS nombre_area,
                    car.nombre AS nombre_cargo,
                    pro.nombre AS nombre_proyecto,
                    UPPER(pro.nombre_corto) AS nombre_corto_proyecto,
                    sed.nombre AS nombre_sede,
                    vis.hora_ingreso,
                    vis.hora_salida
                FROM
                    tbl_visita AS vis
                    LEFT JOIN tbl_persona AS per ON vis.dni=per.dni
                    LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
                    LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
                    LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
                    LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id ";
    }

    // public function select_asistencia(){
    //     return "";
    // }

    // public function select(){
    //     return "SELECT
    //                per.dni,
    //                per.nombres_apellidos,
    //                are.nombre AS nombre_area,
    //                car.nombre AS nombre_cargo,
    //                pro.nombre AS nombre_proyecto,
    //                UPPER(pro.nombre_corto) AS nombre_corto_proyecto,
    //                sed.nombre AS nombre_sede
    //             FROM
    //                tbl_persona AS per
    //                LEFT JOIN tbl_sede AS sed ON per.tbl_sede_id=sed.id
    //                LEFT JOIN tbl_proyecto AS pro ON per.tbl_proyecto_id=pro.id
    //                LEFT JOIN tbl_cargo AS car ON per.tbl_cargo_id=car.id
    //                LEFT JOIN tbl_area AS are ON car.tbl_area_id=are.id ";
    // }

}
