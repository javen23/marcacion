<section id="content">
    <div class="container">
        <div class="block-header p-0">
            <ul class="actions right-0">
                <li data-toggle="tooltip" data-placement="bottom" title="DESCARGAR VISITA">
                    <a href="javascript:;" class="exportar_tabla_reporte"><i class="zmdi zmdi-cloud-download mdc-text-green"></i></a>
                </li>
            </ul>
            <form action="<?php echo base_url('ajax-exportar-tabla') ?> " method="post" target="_blank" id="form_exportar_tabla">
                <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
                <input type="hidden" name="nombre_archivo" value="<?php echo url_amigable(notildes($titulo)) ?>">
                <input type="hidden" name="nombre_fecha" id="nombre_fecha" value="<?php echo date('Y-m-d') ?>">
            </form>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header" id="format-dates">
                        Reporte de asistencia del día <i class="zmdi zmdi-time mdc-text-green-A700"></i> <strong>"<span class="fecha"></span>"</strong> 
                    </div>
                    <div class="card-body card-padding">
                        <div class="row">
                            <div class="col-sm-12 p-20">

                                <div class="col-sm-8 " >
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <label for="listado_proyectos">PROYECTO</label>
                                            <div class="select">
                                                <select class="selectpicker" data-live-search="true" name="listado_proyectos" id="listado_proyectos" multiple>
                                                    <?php if(count($listado_proyecto)>0) : ?>
                                                    <?php foreach ($listado_proyecto as $proyecto) : ?>
                                                        <option value="<?php echo $proyecto['id'] ?>"><?php echo $proyecto['nombre_corto'] ?></option>
                                                    <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2" >
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <label for="fecha_hasta">FECHA</label>
                                            <div class="input-group form-group">
                                                <span class="input-group-addon " ><i class="zmdi zmdi-calendar zmdi-hc-2x"></i></span>
                                                <div class="dtp-container dropdown fg-line">
                                                    <input type="text" id="fecha" name="fecha" value="<?php echo $fecha; ?>" class="form-control input-sm date-picker-span" data-toggle="dropdown"  placeholder="Click aqui...">
													<script>
													$("#fecha").on("dp.change", function(e) {
														$(".fecha").text(moment("'"+$(this).val()+"'","DDMMYYYY").format('dddd, DD [de] MMMM [del] YYYY'));
													})
													</script>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-2">
                                    <div class="form-group">
                                        <div class="fg-line">
                                            <button type="button" class="btn btn-success waves-effect btn-icon-text" id="btn_xyz_asistencia_rango"><i class="zmdi zmdi-filter-list"></i> FILTRAR</button>
											<script>
												$("#btn_xyz_asistencia_rango").on('click',function(){
													//var areas       = $("#listado_area").val();
                                                    var proyecto       = $("#listado_proyectos").val();
													var fecha    = $("#fecha").val();
													//var fecha_hasta = $("#fecha_hasta").val();
													//var sedes       = $("#listado_sedes").val();
													//?sedes='+sedes+'&areas='+areas+'&
													var url         = CI.base_url+'xyz/asistencia_dia/filtro?proyecto='+proyecto+'&fecha='+fecha;
													//console.log(url);
													//alert(fecha_de + "\n" + fecha_hasta + "\n" + url);
													//traer_resultado_tabla(url,'btnFiltrarListadoRango');
													$("#resultado_filtro").slideUp('high', function () {
														$(this).load(url, function () {
															tablaListadoDataTable();
															$(this).slideDown('slow');
															$("#btn_xyz_asistencia_rango").removeClass('disabled');
														});
													});
												});
											</script>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-sm-12 ">
                                <div class="table-responsive row-none" id="resultado_filtro">
                                    <table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                                        <thead class="headTablaListado">
											<tr>
												<th colspan="10" style="font-size:18px !important;">
													ASISTENCIA EN SEDE SALESIANO
												</th>
											</tr>
											<tr>
												<th colspan="1">

												</th>
												<th colspan="8" style="font-size:14px; text-align:center;">
													EVALUACIÓN PARA EL PRIMER CONCURSO DE ACCESO A CARGOS DIRECTIVOS DE UGEL Y DRE Y PRIMER CONCURSO PÚBLICO PARA EL ASCENSO DE LA PRIMERA A LA   SEGUNDA ESCALA MAGISTERIAL DEL CARRERA PÚBLICA MAGISTERIAL DE LA LEY DE REFORMA MAGISTERIAL - 2016
												</th>
												<th colspan="1" style="text-align:right;">
													FECHA: <?php echo str_replace("-", "/", $fecha); ?>
												</th>
											</tr>
                                            <tr class="text-uppercase th-head-inputs">
                                                <th>N°</th>
                                                <th>PROYECTO</th>
                                                <th>AREA</th>
                                                <th>APELLIDOS Y NOMBRES</th>
                                                <th>DNI</th>
												<!--<th>FECHA</th>-->
												<th>HORA ENTRADA</th>
												<th>FIRMA</th>
												<th>HORA SALIDA</th>
												<th>FIRMA</th>
                                                <th>OBSERVACIÓN</th>
                                            </tr>
	                                        </thead>
                                        <tfoot class="footTablaListado">
                                            <tr class="text-uppercase">
                                                <th>N°</th>
                                                <th>PROYECTO</th>
                                                <th>AREA</th>
                                                <th>APELLIDOS Y NOMBRES</th>
                                                <th>DNI</th>
												<!--<th>FECHA</th>-->
												<th>HORA ENTRADA</th>
												<th>FIRMA</th>
												<th>HORA SALIDA</th>
												<th>FIRMA</th>
                                                <th>OBSERVACIÓN</th>
                                            </tr>
                                        </tfoot>
                                        <tbody class="bodyTablaListado">
                                            <?php $n=1; if(count($listado_visita)>0 ) : ?>
                                            <?php foreach ($listado_visita as $listado) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $n++; ?></td>
                                                    <td><?php echo $listado['nombre_corto_proyecto'] ?></td>
                                                    <td><?php echo $listado['nombre_area'] ?></td>
                                                    <td><?php echo $listado['nombres_apellidos'] ?></td>
                                                    <td class="text-center" style="mso-number-format:\@;"><?php echo $listado['dni'] ?></td>
													<!--<td><?php echo $listado["fecha"] ?></td>-->
													<td><?php echo $listado["hora_ingreso"] ?></td>
													<td></td>
													<td><?php echo $listado["hora_salida"] ?></td>
													<td></td>
                                                    <td></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="modal animated zoomIn" id="visitasModal" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-xg">
                    <div class="modal-content"></div>
                </div>
            </div>
        </div>

    </div>
</section>
<script>
$(function () {
    tablaListadoDataTable();
    //$(".today_from").text(moment().subtract(7,'days').format('dddd, DD [de] MMMM [del] YYYY'));
    //$(".today_to").text(moment().format('dddd, DD [de] MMMM [del] YYYY'));
	$(".fecha").text(moment().format('dddd, DD [de] MMMM [del] YYYY'));
});
</script>
