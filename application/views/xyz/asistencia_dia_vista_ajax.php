<table width="100%" id="tablaListado" class="table table-bordered table-striped table-hover" >
                                        <thead class="headTablaListado">
											<tr>
												<th colspan="10">
													ASISTENCIA EN SEDE SALESIANO
												</th>
											</tr>
											<tr>
												<th colspan="1">
													
												</th>
												<th colspan="8">
													EVALUACIÓN PARA EL PRIMER CONCURSO DE ACCESO A CARGOS DIRECTIVOS DE UGEL Y DRE Y PRIMER CONCURSO PÚBLICO PARA EL ASCENSO DE LA PRIMERA A LA   SEGUNDA ESCALA MAGISTERIAL DEL CARRERA PÚBLICA MAGISTERIAL DE LA LEY DE REFORMA MAGISTERIAL - 2016
												</th>
												<th colspan="1">
													FECHA: <?php echo str_replace("-", "/", $fecha); ?>
												</th>
											</tr>
                                            <tr class="text-uppercase th-head-inputs">
                                                <th>N°</th>
                                                <th>PROYECTO</th>
                                                <th>AREA</th>
                                                <th>APELLIDOS Y NOMBRES</th>
                                                <th>DNI</th>
												<!--<th>FECHA</th>-->
												<th>HORA ENTRADA</th>
												<th>FIRMA</th>
												<th>HORA SALIDA</th>
												<th>FIRMA</th>
                                                <th>OBSERVACIÓN</th>
                                            </tr>
	                                        </thead>
                                        <tfoot class="footTablaListado">
                                            <tr class="text-uppercase">
                                                <th>N°</th>
                                                <th>PROYECTO</th>
                                                <th>AREA</th>
                                                <th>APELLIDOS Y NOMBRES</th>
                                                <th>DNI</th>
												<!--<th>FECHA</th>-->
												<th>HORA ENTRADA</th>
												<th>FIRMA</th>
												<th>HORA SALIDA</th>
												<th>FIRMA</th>
                                                <th>OBSERVACIÓN</th>
                                            </tr>
                                        </tfoot>
                                        <tbody class="bodyTablaListado">
                                            <?php $n=1; if(count($listado_visita)>0 ) : ?>
                                            <?php foreach ($listado_visita as $listado) : ?>
                                                <tr>
                                                    <td class="text-center"><?php echo $n++; ?></td>
                                                    <td><?php echo $listado['nombre_corto_proyecto'] ?></td>
                                                    <td><?php echo $listado['nombre_area'] ?></td>
                                                    <td><?php echo $listado['nombres_apellidos'] ?></td>
                                                    <td class="text-center" style="mso-number-format:\@;"><?php echo $listado['dni'] ?></td>
													<!--<td><?php echo $listado["fecha"] ?></td>-->
													<td><?php echo $listado["hora_ingreso"] ?></td>
													<td></td>
													<td><?php echo $listado["hora_salida"] ?></td>
													<td></td>
                                                    <td></td>
                                                </tr>
                                            <?php endforeach; ?>
                                            <?php endif; ?>
                                        </tbody>
                                    </table>